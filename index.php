<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="pragma" content="no-cache">

    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css">

    <title>WEBILITY</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
    <div class="container">
        <p class="navbar-brand mb-0">WEBILITY</p>
    </div>
</nav>

<!-- Full Page Image Header with Vertically Centered Content -->
<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active"
                 style="background: linear-gradient(to bottom, rgba(181, 189, 200, 0.9) 0%,rgba(130, 140, 149, 0.9) 36%,rgba(40, 52, 59, 1) 100%), url('imgs/slide-sites.jpg');">
                <div class="carousel-caption d-flex align-items-end">
                    <h1 class="font-weight-bold text-white text-center w-100">
                        Crie um site para sua empresa ou projeto e gere mais leads!
                    </h1>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item"
                 style="background: linear-gradient(to bottom, rgba(181, 189, 200, 0.9) 0%,rgba(130, 140, 149, 0.9) 36%,rgba(40, 52, 59, 1) 100%), url('imgs/slide-ecommerce.jpg');">
                <div class="carousel-caption d-flex align-items-end">
                    <h1 class="font-weight-bold text-white text-center w-100">
                        Crie uma loja virtual e venda 24 horas por dia e 7 dias por semana!
                    </h1>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item"
                 style="background: linear-gradient(to bottom, rgba(181, 189, 200, 0.9) 0%,rgba(130, 140, 149, 0.9) 36%,rgba(40, 52, 59, 1) 100%), url('imgs/slide-aplicativos.jpg');">
                <div class="carousel-caption d-flex align-items-end">
                    <h1 class="font-weight-bold text-white text-center w-100">
                        Crie um aplicativo e construa experiências corporativas mais significativas!
                    </h1>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>

<section class="services bg-light">
    <div class="container py-5">
        <div class="row align-items-center">
            <div class="col-lg-4 mb-5 mb-lg-0 text-center">
                <i class="fas fa-desktop fa-4x text-warning mb-3"></i>
                <h3 class="text-dark text-center font-weight-bold">
                    Publicidade
                </h3>
                <p class="lead mb-0 text-dark">
                    Um site é mais amigo do ambiente quando se trata de publicidade e marketing.
                </p>
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0 text-center">
                <i class="fas fa-shopping-cart fa-4x text-warning mb-3"></i>
                <h3 class="text-dark font-weight-bold">
                    Variedade de produtos
                </h3>
                <p class="lead mb-0">
                    Maior variedade de produtos é sinônimo de mais vendas.
                </p>
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0 text-center">
                <i class="fas fa-mobile-alt fa-4x text-warning mb-3"></i>
                <h3 class="text-dark font-weight-bold">
                    Fidelizar o cliente
                </h3>
                <p class="lead text-dark mb-0">
                    Mantenha seu cliente engajado com sua marca.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="bg-white">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('imgs/bg-showcase-design-responsivo.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2 class="font-weight-bold">Design totalmente responsivo</h2>
                <p class="lead mb-0">
                    O site ficará muito bem em qualquer dispositivo, seja telefone, tablet ou desktop. A página se
                    comportará de maneira responsiva!
                </p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 text-white showcase-img"
                 style="background-image: url('imgs/bg-showcase-ecommerce.jpg');"></div>
            <div class="col-lg-6 my-auto showcase-text">
                <h2 class="font-weight-bold">Facilidade para estudar o mercado</h2>
                <p class="lead mb-0">
                    Você pode detectar os resultados da concorrência, seus produtos mais vendidos, as perguntas mais
                    frequentes entre os consumidores, etc.
                </p>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 order-lg-2 text-white showcase-img"
                 style="background-image: url('imgs/bg-showcase-app.jpg');"></div>
            <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2 class="font-weight-bold">Experiência do cliente</h2>
                <p class="lead mb-0">
                    Os usuários de hoje querem se sentir bem com o serviço que estão contratando, criamos uma
                    experiência única para seu serviço ou produto.
                </p>
            </div>
        </div>
    </div>
</section>
<section class="bg-light py-5">
    <div class="container">
        <h2 class="text-center mb-5 font-weight-bold">
            Pronto para começar?
        </h2>
        <div class="row">
            <div class="col-lg-4 mb-3 text-center">
                <span class="fa-stack fa-2x mb-3 text-center">
                    <i class="fas fa-square fa-stack-2x text-warning"></i>
                    <i class="fas fa-map-marker-alt fa-stack-1x fa-inverse text-dark"></i>
                </span>
                <h4 class="text-dark font-weight-bold">Belo Horizonte / MG</h4>
            </div>
            <div class="col-lg-4 mb-3 text-center">
                <span class="fa-stack fa-2x mb-3 text-center">
                    <i class="fas fa-square fa-stack-2x text-warning"></i>
                    <i class="fab fa-whatsapp fa-stack-1x fa-inverse text-dark"></i>
                </span>
                <h4 class="text-dark font-weight-bold">
                    <a href="https://api.whatsapp.com/send?phone=5531998216427" target="_blank" class="text-dark">
                        +55 31 99821-6427
                    </a>
                </h4>
            </div>
            <div class="col-lg-4 mb-3 text-center">
                <span class="fa-stack fa-2x mb-3 text-center">
                    <i class="fas fa-square fa-stack-2x text-warning"></i>
                    <i class="fas fa-at fa-stack-1x fa-inverse text-dark"></i>
                </span>
                <h4 class="text-dark font-weight-bold">
                    <a href="mailto:contato@webility.com.br" target="_blank" class="text-dark">
                        contato@webility.com.br
                    </a>
                </h4>
            </div>
        </div>
        <form action="mail.php" method="post">
            <div class="form-group">
                <input type="text" name="nome" id="nome" class="form-control" required placeholder="Nome: *">
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control" required placeholder="E-mail: *">
            </div>
            <div class="form-group">
                <input type="number" name="telefone" id="telefone" class="form-control" required
                       placeholder="Telefone: *" minlength="10" maxlength="11">
            </div>
            <div class="form-group">
                <select name="produto" id="produto" class="form-control" required>
                    <option value="">Produto: *</option>
                    <option value="Aplicativo">Aplicativo</option>
                    <option value="Loja Virtual">Loja Virtual</option>
                    <option value="Site">Site</option>
                </select>
            </div>
            <div class="form-group">
                <textarea name="mensagem" id="mensagem" class="form-control" placeholder="Mensagem: *"
                          required></textarea>
            </div>
            <div class="form-group mb-0 text-right">
                <button type="submit" class="btn btn-success">
                    Começar!
                </button>
            </div>
        </form>
    </div>
</section>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
